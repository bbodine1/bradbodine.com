+++
title = "About"
date = 2017-08-15T13:50:21-05:00

type = "page"
layout = "layout-1"

pageClass = ""

menu = "main"
weight = 40

draft = false
+++
This is the about page. I will add more about me here soon. If you'd like to know more about who I am and what I can do for your business, please just send me a message, either through [Facebook](https://m.me/bradbodine.ads) or just click the button above that says "Start Getting Customers".

Thanks for taking the time to check me out and I look forward to working with you!

Brad